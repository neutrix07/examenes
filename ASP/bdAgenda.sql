/*creamos la Base de Datos*/
CREATE DATABASE agenda;
/*usamos la base de datos*/
USE agenda;
/*creamos la tabla contacto*/
CREATE TABLE contacto(
	id_contacto INT NOT NULL PRIMARY KEY IDENTITY(1,1),
	nombre nvarchar(50),
	telefono INT,
	correo NVARCHAR(30),
	fecha_nacimiento DATE,
	notas TEXT
);

/*insertamos 2 registtros*/
INSERT INTO cliente values('Oscar Mendoza', 10000, 26, 'oscar_mdp10@gmail.com');
INSERT INTO cliente values('Luis CG', 11000, 24, 'luiscg@gmail.com');

