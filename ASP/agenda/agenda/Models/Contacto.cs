﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
/*Mapeo de la Base de Datos con Entity Framework*/
namespace agenda.Models
{
    public class Contacto{
    //llave primaria
        [Key]
        public int id_contacto { get; set; }

    //nombre de contacto
        [Required(ErrorMessage = "Ingrese nombre de contacto")]
        [Display(Name = "Nombre de Contacto")]
        public string Nombre { get; set; }
    
    //telefono de contacto
        [Required(ErrorMessage = "Ingrese telefono de contacto valido (8 digitos)")]
        [Display(Name = "Telefono (8 digitos)")]
        public int Telefono { get; set; }
    
    //correo de contacto
        [Required(ErrorMessage = "Ingrese un correo valido")]
        [Display(Name = "Correo")]
        [EmailAddress(ErrorMessage = "Ingrese un correo valido")]
        public string Correo { get; set; }

    //fecha de nacimiento del contacto
        [Required(ErrorMessage = "Ingrese una fecha valida")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Fecha Nacimiento")]
        public System.DateTime fecha_nacimiento { get; set; }

    //notas del contacto
        [Display(Name = "Notas")]
        public string Notas { get; set; }
    }
}
