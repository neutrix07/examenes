﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

using agenda.Models;
using Microsoft.EntityFrameworkCore;
namespace agenda.Controllers
{
    public class ContactoController : Controller
    {
        private readonly ApplicationDbContext _db;
        public ContactoController(ApplicationDbContext db)
        {
            _db = db;
        }

        public IActionResult Index()
        {
            var datos_lista = _db.Contacto.ToList();
            return View(datos_lista);
        }

    //lanza vista creacion de contacto
        public IActionResult Agregar()
        {
            return View();
        }
    //inserta nuevo contacto
        [HttpPost]
        public async Task<IActionResult> Agregar(Contacto contacto)
        {
            if (ModelState.IsValid)
            {
                _db.Add(contacto);
                await _db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(contacto);
        }

//detalle de contacto
        public async Task<IActionResult> Detalle(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }

            var getDetalleContacto = await _db.Contacto.FindAsync(id);
            return View(getDetalleContacto);
        }

    //edicion de cliente
        public async Task<IActionResult> Editar(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }

            var getContDetalle = await _db.Contacto.FindAsync(id);
            return View(getContDetalle);
        }
        //este metodo sirve para guardar los datos en el update
        [HttpPost]
        public async Task<IActionResult> Editar(Contacto oldContacto)
        {
            if (ModelState.IsValid)
            {
                _db.Update(oldContacto);
                await _db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(oldContacto);
        }

    //lanza vista eliminar contacto
        public async Task<IActionResult> Eliminar(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }
            var detalleContacto = await _db.Contacto.FindAsync(id);
            return View(detalleContacto);
        }

        [HttpPost]
        public async Task<IActionResult> Eliminar(int id)
        {

            var getContactoDetalle = await _db.Contacto.FindAsync(id);
            _db.Contacto.Remove(getContactoDetalle);
            await _db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

//Busqueda de contactos por nombre o correo
        [HttpGet]
        public async Task<IActionResult> Index(string busquedaContacto)
        {
            ViewData["GetContactoDetalle"] = busquedaContacto;
            var empquery = from x in _db.Contacto select x;
            if (!String.IsNullOrEmpty(busquedaContacto))
            {
                empquery = empquery.Where(x => x.Nombre.Contains(busquedaContacto)
                || x.Correo.Contains(busquedaContacto));
            }
            return View(await empquery.AsNoTracking().ToListAsync());
        }
    }
}
